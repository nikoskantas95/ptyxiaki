import subprocess
from statistics import mean
import numpy as np
import matplotlib.pyplot as plt
import os


free=list(range(0,5));
reuse=list(range(0,5));





#	calculating with hazard pointers
with open('v7_final_msq_hazard.txt',"r") as f:
	i=1;
	print("\tCreating graphs for MSQueue\t");
	while(i<6):
		#print("Calculating hazard pointers case using ",2**i," threads ...");
		results=list(range(0,10));
		for x in range(10):
			results[x]=int(f.readline().strip("\n"));
		#print(results);
		results.remove(max(results));
		results.remove(min(results));
		free[i-1]=mean(results);
		i=i+1;
with open('v9_msq_hazard.txt',"r") as f:
	i=1;
	print("\tCreating graphs for MSQueue\t");
	while(i<6):
		#print("Calculating hazard pointers case using ",2**i," threads ...");
		results=list(range(0,10));
		for x in range(10):
			results[x]=int(f.readline().strip("\n"));
		#print(results);
		results.remove(max(results));
		results.remove(min(results));
		reuse[i-1]=mean(results);
		i=i+1;

with open('out.txt',"w") as fl:
	fl.write("Free")
	for item in free:
		fl.write(','+str(item));

with open('out.txt',"a") as fl:
	fl.write("\n"+"Reuse")
	for item in reuse:
		fl.write(','+str(item));



print(free);
print(reuse);
