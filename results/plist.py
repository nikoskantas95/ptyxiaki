import sys
import subprocess
from statistics import mean
import numpy as np
import matplotlib.pyplot as plt

plist=list(range(0,5));
no_plist=list(range(0,5));
#	calculating plist
i=1;
while(i<6):
	print("Calculating plist case using ",2**i," threads ...");
	results=list(range(0,10));
	for x in range(10):			
		proc=subprocess.Popen(['./plist.run',str(i*2)],stdout=subprocess.PIPE,stdin=subprocess.PIPE);		
		results[x]=int(proc.stdout.read());		
	results.remove(max(results));
	results.remove(min(results));
	plist[i-1]=mean(results);
	i=i+1;
#	calculating no_plist
i=1;
while(i<6):
	print("Calculating no_plist case using ",2**i," threads ...");
	results=list(range(0,10));
	for x in range(10):			
		proc=subprocess.Popen(['./no_plist.run',str(i*2)],stdout=subprocess.PIPE,stdin=subprocess.PIPE);		
		results[x]=int(proc.stdout.read());		
	results.remove(max(results));
	results.remove(min(results));
	no_plist[i-1]=mean(results);
	i=i+1;
#	calculating no_temp
'''i=1;
while(i<6):
	print("Calculating no rlist_temp case using ",2**i," threads ...");
	results=list(range(0,3));
	for x in range(3):			
		proc=subprocess.Popen(['./no_temp.run',str(i*2)],stdout=subprocess.PIPE,stdin=subprocess.PIPE);		
		results[x]=int(proc.stdout.read());		
	results.remove(max(results));
	results.remove(min(results));
	no_temp[i-1]=mean(results);
	i=i+1;'''



print("Exporting stats to png file ...");

# data to plot
n_groups = 5 
# create plot
fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = 0.35
opacity = 0.8
 
rects1 = plt.bar(index, plist, bar_width,
                 alpha=opacity,
                 color='b',
                 label='Using plist');
 
rects2 = plt.bar(index + bar_width, no_plist, bar_width,
                 alpha=opacity,
                 color='r',
                 label='Using no plist');

'''rects3 = plt.bar(index + 2*bar_width, no_temp, bar_width,
                 alpha=opacity,
                 color='r',
                 label='Using plist but no rlist_temp');'''
 
plt.xlabel('Number of Threads')
plt.ylabel('Time (ms)')
plt.title('Average time of execution (ms)')
plt.xticks(index + bar_width, ('2', '4', '8', '16','32'));
plt.legend() 
plt.tight_layout()
print("Complete!");
print("\t-------------------- Results --------------------");
print("Plist times:\n"+str(plist)); 
print("No plist times:\n"+str(no_plist)); 
plt.savefig('plist.png')