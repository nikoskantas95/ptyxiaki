import subprocess
from statistics import mean
import numpy as np
import matplotlib.pyplot as plt
import os


v7_msQ_10m_80_20_allthreads=list(range(0,32));
v7_msQ_10m_60_40_allthreads=list(range(0,32));
v9_msQ_10m_80_20_allthreads=list(range(0,32));
v9_msQ_10m_60_40_allthreads=list(range(0,32));







#	collecting data
with open('v7_msQ_10m_80_20_allthreads.txt',"r") as f:
	i=1;
	while(i<32):
		#print("Calculating hazard pointers case using ",2**i," threads ...");
		results=list(range(0,10));
		for x in range(10):
			results[x]=int(f.readline().strip("\n"));
		#print(results);
		results.remove(max(results));
		results.remove(min(results));
		v7_msQ_10m_80_20_allthreads[i-1]=mean(results);
		i=i+1;

with open('v7_msQ_10m_60_40_allthreads.txt',"r") as f:
	i=1;
	while(i<32):
		#print("Calculating hazard pointers case using ",2**i," threads ...");
		results=list(range(0,10));
		for x in range(10):
			results[x]=int(f.readline().strip("\n"));
		#print(results);
		results.remove(max(results));
		results.remove(min(results));
		v7_msQ_10m_60_40_allthreads[i-1]=mean(results);
		i=i+1;


with open('v9_msQ_10m_80_20_allthreads.txt',"r") as f:
	i=1;
	while(i<32):
		#print("Calculating hazard pointers case using ",2**i," threads ...");
		results=list(range(0,10));
		for x in range(10):
			results[x]=int(f.readline().strip("\n"));
		#print(results);
		results.remove(max(results));
		results.remove(min(results));
		v9_msQ_10m_80_20_allthreads[i-1]=mean(results);
		i=i+1;

with open('v9_msQ_10m_60_40_allthreads.txt',"r") as f:
	i=1;
	while(i<32):
		#print("Calculating hazard pointers case using ",2**i," threads ...");
		results=list(range(0,10));
		for x in range(10):
			results[x]=int(f.readline().strip("\n"));
		#print(results);
		results.remove(max(results));
		results.remove(min(results));
		v9_msQ_10m_60_40_allthreads[i-1]=mean(results);
		i=i+1;



##################################################################
##################################################################
##################################################################


#flushing data



with open('results_allthreads.txt',"w") as fl:
	fl.write("v7_msQ_10m_80_20_allthreads")
	for item in v7_msQ_10m_80_20_allthreads:
		fl.write(','+str(item));

with open('results_allthreads.txt',"a") as fl:
	fl.write("\n"+"v9_msQ_10m_80_20_allthreads")
	for item in v9_msQ_10m_80_20_allthreads:
		fl.write(','+str(item));

with open('results_allthreads.txt',"a") as fl:
	fl.write("\n\nv7_msQ_10m_60_40_allthreads")
	for item in v7_msQ_10m_60_40_allthreads:
		fl.write(','+str(item));

with open('results_allthreads.txt',"a") as fl:
	fl.write("\n"+"v9_msQ_10m_60_40_allthreads")
	for item in v9_msQ_10m_60_40_allthreads:
		fl.write(','+str(item));
