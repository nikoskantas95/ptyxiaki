#include <hazard_utils.h>

void merge(void *arr[], int l, int m, int r)
{
   
    int i=0, j=0, k=0;
    int n1 = m - l + 1;
    int n2 =  r - m; 
 
    /* create temp arrays */
    void *L[n1] , *R[n2]  ;
 
    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[m + 1+ j];
 
    /* Merge the temp arrays back into arr[l..r]*/
    i = 0; // Initial index of first subarray
    j = 0; // Initial index of second subarray
    k = l; // Initial index of merged subarray
    while (i < n1 && j < n2)
    {
        if (L[i] <= R[j])
        {
            arr[k] = L[i];
            i++;
        }
        else
        {
            arr[k] = R[j];
            j++;
        }
        k++;
    }
 
    /* Copy the remaining elements of L[], if there
       are any */
    while (i < n1)
    {
        arr[k] = L[i];
        i++;
        k++;
    }
 
    /* Copy the remaining elements of R[], if there
       are any */
    while (j < n2)
    {
        arr[k] = R[j];
        j++;
        k++;
    }
}


void mergeShort(void *array[],int low,int high){
  //  printf("Trying to merge\n");
	int mid;
	if(low < high) {
      mid = (low + high) / 2;   
      mergeShort(array,low, mid);
      mergeShort(array,mid+1, high);
      merge(array,low, mid, high);
   } else { 
   // printf("Merge success\n");
      return;
   }   
}

void swap(void **xp, void **yp)
{
    void *temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void bubbleSort(void *array[],int size){
   int i, j;
   for (i = 0; i < size-1; i++)      
 
       // Last i elements are already in place   
       for (j = 0; j < size-i-1; j++) 
           if (array[j] > array[j+1])
              swap(&array[j], &array[j+1]);
}

/*  comperator for quickshort */
int comperator (const void * a, const void * b) {
     if(a < b){
    //printf("a < b\n");
      return -1;
   }
    else if (a > b){
     // printf("a > b\n");
      return 1;
    }
    else{
      //printf("a = b\n");
      return 0;
    }
}

int partition (void *arr[], int low, int high)
{
    int j;
    void *pivot = arr[high];    // pivot
    int i = (low - 1);  // Index of smaller element
 
    for (j = low; j <= high- 1; j++)
    {
        // If current element is smaller than or
        // equal to pivot
        if (arr[j] <= pivot)
        {
            i++;    // increment index of smaller element
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}



void quickSort(void *arr[], int low, int high)
{
    if (low < high)
    {
        /* pi is partitioning index, arr[p] is now
           at right place */
        int pi = partition(arr, low, high);
 
        // Separately sort elements before
        // partition and after partition
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}
