#ifndef _BUCKET_H_
#define _BUCKET_H_

#include <primitives.h>
#include <queue-stack.h>
#include <primitives.h>

volatile int buckets_freed;
volatile int total_buckets;
volatile int total_nodes_reused;


typedef struct bucketStruct{
	char memory_pool[8192*sizeof(Node)];
	volatile int memory_counter; //counts total chuncks of memory being used
	int memory_bitmap[8192];
	int index; // current index at memory_pool.Must be different from memory_counter
	//for concurrency issues.Index shows how big the memory_pool currently is.
	//Memory counter shows how many indexes in the memory_pool can be used
	int obj_size;  //used in alloc_object
	int id; //for debugging purposes
	int allignment;
}bucketStruct;

typedef struct bucketList{

  bucketStruct *bucket;
	struct bucketList *next;
	struct bucketList *prev;

}bucketList;



void create_bucket(bucketList **blist,int obj_size);
void *alloc_object(bucketList **blist);
void insertBucket(bucketList **blist,bucketStruct *new_bucket);
//void *getAddress(void *address,int allignment,int offset); --legacy
//void deleteBucket(bucketList **blist);--legacy
int get_bucketAllignment(int allignment);
void cross(bucketList *blist);
//void *realloc_object(bucketList **blist);
int get_memoryBitmap_index(int bitmap[]);
bucketList *getAvailableBucket(bucketList **blist);
void bucket_reuse_memory(void *address,int allignment,int offset);
void error_checking(bucketList *blist);
void increment_memory_counter(void *memory_counter_cnt);


#endif
