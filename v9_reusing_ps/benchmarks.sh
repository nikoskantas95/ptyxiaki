#!/bin/bash

#pairs
POWER=1;
for ((i=2;i<64;i=2**POWER)) ;do
	echo "MSqueue operations in pairs using $i threads ...";
	for((j=1;j<=10;j++)) ;do
		echo -n "#";
		./bin/test1_hazard_10m_bench.run $i >> v9_msQ_pairs.txt;

	done;
	((POWER++));
	echo -e "\n";
done;

#60-40
POWER=1;
for ((i=2;i<64;i=2**POWER)) ;do
	echo "MSqueue 60-40 using $i threads ...";
	for((j=1;j<=10;j++)) ;do
		echo -n "#";
		./bin/test1_hazard_10m60-40_bench.run $i >> v9_msQ_60-40.txt;

	done;
	((POWER++));
	echo -e "\n";
done;

#80-20
POWER=1;
for ((i=2;i<64;i=2**POWER)) ;do
	echo "MSqueue 60-40 using $i threads ...";
	for((j=1;j<=10;j++)) ;do
		echo -n "#";
		./bin/test1_hazard_10m80-20_bench.run $i >> v9_msQ_80-20.txt;

	done;
	((POWER++));
	echo -e "\n";
done;
