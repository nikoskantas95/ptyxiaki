#!/bin/bash
POWER=1;
for ((i=2;i<64;i=2**POWER)) ;do
	echo "Calculating MSqueue using hazard pointers case with $i threads ..."; 
	for((j=1;j<=10;j++)) ;do
		echo -n "#";
		./bin/test1_hazard_bench.run $i >> v8_msq_hazard.txt;
		
	done;
	((POWER++));
	echo -e "\n";
done;


POWER=1;
for ((i=2;i<64;i=2**POWER)) ;do
	echo "Calculating LFStack using hazard pointers case with $i threads ..."; 
	for((j=1;j<=10;j++)) ;do
		echo -n "#";
		./bin/test1_lfhazard_bench.run $i >> v8_hazard.txt;
		
	done;
	((POWER++));
	echo -e "\n";
done;
