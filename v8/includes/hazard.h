#ifndef _HAZARD_H_
#define _HAZARD_H_
#include <stdio.h>
#include <unistd.h>
#include <string.h> 
#include <stdlib.h>
#include <pthread.h>
#include <primitives.h>
#include <bucket.h>


//void ***HPRecords;
int total_instances  __attribute__ ((aligned (128)));

typedef struct HPStructure{
	void **HPRecords; //array containing system's hp
	//int total_instances  __attribute__ ((aligned (64)));
	int size;
} HPStructure;

typedef struct retiredList{
	void *node;	
	struct retiredList *next;
	struct retiredList *prev;
} retiredList;

volatile HPStructure HPStruct;
void initHPRec(int thread_n,int instances);
void pushRetired(retiredList **head,void *node);
retiredList *popRetired(retiredList **head);
void addRetired(retiredList **head,void *node);
void deleteRetired(retiredList **rnode);
void popAllRetired(retiredList **rlist,retiredList **tmplist);
int lookUpHP(void *plist[],void *node,int size);
int lookUpHPBinary(void *array[],void *node,int start,int size);
void Scan(retiredList **rlist,int *rcount,int thread_num);
void RetireNode(void *node,retiredList **rlist,int *rcount,int thread_num,bucketList **blist);

#endif