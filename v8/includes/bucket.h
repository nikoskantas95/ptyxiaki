#ifndef _BUCKET_H_
#define _BUCKET_H_

#include <primitives.h>
#include <queue-stack.h>
#include <primitives.h>

typedef struct bucketStruct{
	char memory_pool[8192*sizeof(Node)];
	int available_zones;
//	char *heap;
	int index;
	int obj_size;
	int id;
	//char padding[131060];
	//int size=sizeof(bucketStruct);
}bucketStruct;



typedef struct bucketList{
    bucketStruct *bucket;
	struct bucketList *next;
	
}bucketList;


void init_bucket(bucketList **blist,int obj_size);
void *alloc_object(bucketList **blist);
void insertBucket(bucketList **blist,bucketStruct *new_bucket);
void *getAddress(void *address,int allignment,int offset);
void deleteZone(void *counter);
void deleteBucket(bucketList **blist);
int getAllignment(int allignment);
void cross(bucketList *blist);



#endif