#include <bucket.h>
#include <math.h>

const int MEM_SIZE = 8192;


void init_bucket(bucketList **blist,int obj_size){
	static int b_id=0;
	b_id++;
	bucketStruct *new_bucket;
	int allignment=getAllignment(sizeof(bucketStruct));
	new_bucket=getAlignedMemory(allignment,sizeof(bucketStruct));
	
	new_bucket->available_zones=MEM_SIZE;
	new_bucket->obj_size=obj_size;
	new_bucket->index=0;
	new_bucket->id=b_id;
	
	insertBucket(blist,new_bucket);
}

void *alloc_object(bucketList **blist){	
	int offset;
	if((*blist)->bucket->index==MEM_SIZE){	
		int size=(*blist)->bucket->obj_size;
		init_bucket(blist,size);
	}
	offset=(*blist)->bucket->index;	
	(*blist)->bucket->index += 1;

	long int val= (void *) &(*blist)->bucket->memory_pool[offset * (*blist)->bucket->obj_size];	
	return (void *) &(*blist)->bucket->memory_pool[offset * (*blist)->bucket->obj_size];
}






void insertBucket(bucketList **blist,bucketStruct *new_bucket){	
	bucketList *temp=NULL,*prev=NULL;
	temp=getAlignedMemory(CACHE_LINE_SIZE,sizeof(bucketList));
	temp->bucket=new_bucket;

	if(blist==NULL){		
		temp->next=NULL;
		*blist=temp;
	}
	else{
		temp->next=*blist;
		*blist=temp;
		
	}
}

void *getAddress(void *address,int allignment,int offset){
	long int aux=((long int)address);
	void *val;
	aux=aux/allignment;	
	aux=aux*allignment;
	val=(void *) (aux + offset*MEM_SIZE);	
	return  val;

}

void deleteZone(void *counter){	
	FAA32(counter,-1);
}

void cross(bucketList *blist){
	while(blist!=NULL){
		printf("Bucket id:%d\n",blist->bucket->id);
		blist=blist->next;
	}
}

void deleteBucket(bucketList **blist){
	bucketList *curr=*blist,*prev;
	if((*blist)->bucket->available_zones==0){
		(*blist)->next=(*blist)->next;
	}
	else{
		while(curr!=NULL && curr->bucket->available_zones!=0){
			prev=curr;			
			curr=curr->next;			
		}
		if(curr==NULL)
			return;		
		prev->next=curr->next;		
		free(curr);
		
	}


	
}

int getAllignment(int allignment){
	float result;
	result=log2(allignment);	
	return pow(2,(int) ceil(result));



}



