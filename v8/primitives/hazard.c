
#include <hazard.h>
#include <hazard_utils.h>
#include <math.h>
#define R 8192*2

/*	Every thread hold somewhere a retired list called rlist containing concurrent hazardous entities */
/*	rlist is implemented as a stack																	 */
/*	HPRecords is a shared array containing each thread's hazard pointers.							 */
/*	Plist is an auxilery array containing the PRESENT pointers of HPRecords.Needs further 			 */
/*	implementation for faster searches																 */


void initHPRec(int thread_n,int instances){
	HPStruct.HPRecords=memalign(128,2*thread_n*instances*sizeof(void*));
	HPStruct.size=2*thread_n*instances;
}


 /* pushes a concurent entity node at the top of the stack */
void pushRetired(retiredList **head,void *node){
	retiredList *temp=memalign(128,sizeof(retiredList));	
	temp->node=node;
	temp->next=NULL;	
	if(*head==NULL){		
		*head=temp;			
	}
	else{
			
		temp->next=*head;
		*head=temp;	
	}	

}

void addRetired(retiredList **head,void *node){
	retiredList *temp=memalign(128,sizeof(retiredList));	
	temp->node=node;
	temp->next=NULL;
	temp->prev=NULL;
	if(*head==NULL){
		*head=temp;
	}	
	else{
		//printf("New head: %p\n",temp);	
		(*head)->prev=temp;
		temp->next=*head;		
		*head=temp;
	}
}

void deleteRetired(retiredList **rnode){
	retiredList *temp=NULL;
	if(*rnode==NULL){		
		return NULL;
	}
	else{
		if((*rnode)->prev==NULL){//rnode is head
			*rnode=(*rnode)->next; 
			(*rnode)->prev==NULL;
		}
		else{
			//temp=*rnode;
			((*rnode)->prev)->next=(*rnode)->next;
			if((*rnode)->next!=NULL) //rnode is not the last element of the stack
				((*rnode)->next)->prev=(*rnode)->prev;
		}
	}
	//return temp;
}
/* pops the next concurent entity */
retiredList *popRetired(retiredList **head){ //isws kapiou eidos free
	retiredList *temp=NULL;
	if(*head==NULL){		
		return NULL;
	}
	else{
		temp=*head;
		*head=temp->next;		
	}	
	return temp;
}


/* auxilery function,moves the concurent entities in rlist into the tmplist */
void popAllRetired(retiredList **rlist,retiredList **tmplist){
	retiredList *aux=NULL;	
	aux=popRetired(rlist);	
	while(aux!=NULL){
		pushRetired(tmplist,aux->node);		
		aux=popRetired(rlist);		
	}	
	
}

int lookUpHP(void *plist[],void *node,int size){
	int i;
	for(i=0;i<size;i++){
		//printf("%p\n",plist[i]);
		if(plist[i]==node){			
			return 1;
		}
	}
	return 0;
}

int lookUpHPRec(void *node,int thread_n){
	int i;
	for(i=0;i<HPStruct.size;i++){
		if(HPStruct.HPRecords[i] == node )
			return 1;
	}
	return 0;
}

/* Checks if node is in plist using binary search.Plist should  */
/* be shorted using mergeShort 									*/
int lookUpHPBinary(void *array[],void *node,int start,int size){
	/*int i;
	for(i=0;i<size;i++){
		if(plist[i]==node){			
			return 1;
		}
	}
	return 0;*/
	int first=start;
	int last=size-1;
	int middle=(first+last)/2;
	while (first <= last) {
      if (array[middle] < node)
         first = middle + 1;    
      else if (array[middle] == node) {
        // printf("%d found at location %p.\n", node, middle+1);
         return 1;
      }
      else
         last = middle - 1;
 
      middle = (first + last)/2;
   }
   if (first > last)
     // printf("Not found! %d isn't present in the list.\n", node);
 
   return 0;   
}

void Scan(retiredList **rlist,int *rcount,int thread_num){
	int hp_num=2*total_instances;	
	int size=thread_num*hp_num; //kanonika * ari8mos HP
	int plist_index=0;	
	int i,j,allingment,zone_offset;
	void *zone;	
	retiredList *tmplist=NULL,*node=NULL,*curr=NULL,*next=NULL;
	curr=*rlist;
	next=curr->next;
	int limit=(*rcount);
	//printf("Start is %p\n",curr);
	while(curr!=NULL){
		if(lookUpHPRec(curr->node,thread_num) == 0){
			allingment=getAllignment(sizeof(bucketStruct));
			zone_offset=sizeof(Node);
			zone=getAddress(curr->node,allingment,zone_offset);			
			deleteZone(zone);
			deleteRetired(&curr);
		}	
		curr=next;
		if(curr!=NULL)
			next=curr->next;
	}
	
}

void RetireNode(void *node,retiredList **rlist,int *rcount,int thread_num,bucketList **blist){		
	retiredList *aux;	
	//printf("Size of hprec %d\n",HPStruct.size);
	addRetired(rlist,node);	
	//exit(0);
	(*rcount)++; 	
	if(*rcount>R){//to do R		
		//printf("Remove\n");
		Scan(rlist,rcount,thread_num);//	
		deleteBucket(blist);
		//cross(*blist);
		
	}	
}
