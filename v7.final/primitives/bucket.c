#include <bucket.h>
#include <math.h>

const int MEM_SIZE = 8192;


void create_bucket(bucketList **blist,int obj_size){
	static int b_id=0;
	//remove from comment for memory values
	//FAA32(&total_buckets,1);
	bucketStruct *new_bucket;
	int allignment,bid;
	if(*blist== NULL){
		allignment=get_bucketAllignment(sizeof(bucketStruct));
		bid=0;
	}
	else{
		allignment=(*blist)->bucket->allignment;
		bid=(*blist)->bucket->id+1;
	}
	new_bucket=getAlignedMemory(allignment,sizeof(bucketStruct));
	new_bucket->memory_counter=MEM_SIZE;
	new_bucket->obj_size=obj_size;
	new_bucket->index=0;
	new_bucket->id=bid;
	new_bucket->allignment=allignment;
	insertBucket(blist,new_bucket);
}

void *alloc_object(bucketList **blist){
	int offset;
	if((*blist)->bucket->index==MEM_SIZE){
		int size=(*blist)->bucket->obj_size;
		create_bucket(blist,size);
	}
	offset=(*blist)->bucket->index;
	(*blist)->bucket->index += 1;
	long int val= (void *) &(*blist)->bucket->memory_pool[offset * (*blist)->bucket->obj_size];
	return (void *) &(*blist)->bucket->memory_pool[offset * (*blist)->bucket->obj_size];
}


void insertBucket(bucketList **blist,bucketStruct *new_bucket){
	bucketList *temp=NULL,*prev=NULL;
	temp=getAlignedMemory(CACHE_LINE_SIZE,sizeof(bucketList));
	temp->bucket=new_bucket;
	temp->next=NULL;
	temp->prev=NULL;
	if((*blist)!= NULL){
		(*blist)->prev=temp;
		temp->next=(*blist);
	}
	*blist=temp;
}

void *get_memory_counter_address(void *address,int allignment,int offset){
	long int aux=((long int)address);
	void *val;
	aux=aux/allignment;
	aux=aux*allignment;
	val=(void *) (aux + offset*MEM_SIZE);
	return  val;

}

/*Marks a chunk of memory to be freed,by decrementing the memory_counter of the
memory pool*/
void markFree_hazardous_memory(void *address,int allignment){
	void * mem=get_memory_counter_address(address,allignment,sizeof(Node));
	FAA32(mem,-1);//FAA32 is a fatch and add implementation not binded by different compiler versions.
}

/*Legacy*/
void decrement_memory_counter(void *counter){

	FAA32(counter,-1); //FAA32 is a fatch and add implementation not binded by different compiler versions.
}

/*debugging purposes*/
void cross(bucketList *blist){
	while(blist!=NULL){
		blist=blist->next;
	}
}


/*Frees any buckets that are available to be freed and have no hazardous refferences*/
void clear_buckets(bucketList **blist){
	bucketList *curr=*blist,*prev,*to_free;
	while(curr!=NULL){
		if(curr->bucket->memory_counter == 0){
			to_free=curr;
			if(curr == *blist){
				(*blist)=(*blist)->next;
				(*blist)->prev=NULL;
				//remove from comment for memory values
				//FAA32(&buckets_freed,1);
				free(to_free);
			}
			else{
				curr->prev->next=curr->next;
				if(curr->next != NULL)
					curr->next->prev=curr->prev;
				 	//remove from comment for memory values
					//FAA32(&buckets_freed,1);
					free(to_free);
			}
		}

		curr=curr->next;
	}




}

int get_bucketAllignment(int allignment){
	//static int cnt;
	float result;
	result=log2(allignment);
	return pow(2,(int) ceil(result));



}
