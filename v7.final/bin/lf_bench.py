import sys
import subprocess
from statistics import mean
import numpy as np
import matplotlib.pyplot as plt

hazard=list(range(0,5));
no_hazard=list(range(0,5));
#	calculating with hazard pointers
i=1;
print("\t -----Testing stack----- '\t");
while(i<6):
	print("Calculating hazard pointers case using ",2**i," threads ...");
	results=list(range(0,10));
	for x in range(10):			
		proc=subprocess.Popen(['./test1_lfhazard_bench.run',str(i**2)],stdout=subprocess.PIPE,stdin=subprocess.PIPE);		
		results[x]=int(proc.stdout.read());		
	results.remove(max(results));
	results.remove(min(results));
	hazard[i-1]=mean(results);
	i=i+1;
print(hazard);
#	calculating without hazard pointers
i=1;
while(i<6):
	print("Calculating non-hazard pointers case using ",2**i," threads ...");
	results=list(range(0,10));
	for x in range(10):			
		proc=subprocess.Popen(['./test1_lf_no_hazard_bench.run',str(i**2)],stdout=subprocess.PIPE,stdin=subprocess.PIPE);		
		results[x]=int(proc.stdout.read());		
	results.remove(max(results));
	results.remove(min(results));
	no_hazard[i-1]=mean(results);
	i=i+1;
print(no_hazard);


print("Exporting stats to png file ...");

# data to plot
n_groups = 5 
# create plot
fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = 0.35
opacity = 0.8
 
rects1 = plt.bar(index, hazard, bar_width,
                 alpha=opacity,
                 color='b',
                 label='LFStack Using Hazard Pointers');
 
rects2 = plt.bar(index + bar_width, no_hazard, bar_width,
                 alpha=opacity,
                 color='g',
                 label='LFStack Without Hazard pointers');
 
plt.xlabel('Number of Threads')
plt.ylabel('Time (ms)')
plt.title('Average time of execution (ms)')
plt.xticks(index + bar_width, ('2', '4', '8', '16','32'));
plt.legend() 
plt.tight_layout()
print("Complete!"); 
plt.savefig('lfstack_results.png')