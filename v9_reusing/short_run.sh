#!/bin/bash
POWER=1;
for ((i=2;i<64;i=2**POWER)) ;do
	echo "MSqueue 60-40 using $i threads ...";
	for((j=1;j<=10;j++)) ;do
		echo -n "#";
		./bin/test1_hazard_10m60-40_bench.run $i >> msQ_60_40.txt;

	done;
	((POWER++));
	echo -e "\n";
done;
