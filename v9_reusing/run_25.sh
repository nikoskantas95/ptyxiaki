#!/bin/bash
POWER=1;
for ((i=2;i<64;i=2**POWER)) ;do
	echo "MSqueue 25m_ops using $i threads ...";
	for((j=1;j<=10;j++)) ;do
		echo -n "#";
		./bin/test1_hazard_25m_bench.run $i >> msQ_25m.txt;

	done;
	((POWER++));
	echo -e "\n";
done;

POWER=1;
for ((i=2;i<64;i=2**POWER)) ;do
	echo "MSqueue 25m_ops 80_20 using $i threads ...";
	for((j=1;j<=10;j++)) ;do
		echo -n "#";
		./bin/test1_hazard_25m80-20_bench.run $i >> msQ_25m_80_20.txt;

	done;
	((POWER++));
	echo -e "\n";
done;

for ((i=8;i<21;i++)) ;do
	echo "MSqueue 25m_ops 80_20(8-20 threads) using $i threads ...";
	for((j=1;j<=10;j++)) ;do
		echo -n "#";
		./bin/test1_hazard_25m80-20_bench.run $i >> msQ_25m_80_20_8-20threads.txt;

	done;

	echo -e "\n";
done;
