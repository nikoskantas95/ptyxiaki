#ifndef _HP_LFSTACK_H_
#define _HP_LFSTACK_H_

#include <config.h>
#include <primitives.h>
#include <backoff.h>
#include <fastrand.h>
//#include <pool.h>
#include <threadtools.h>
#include <queue-stack.h>
#include <hazard.h>
#include <bucket.h>

typedef struct LFStack {
    volatile Node *top;
    int instance  __attribute__ ((aligned (128)));
    int offset;
} LFStack;

typedef struct LFStackThreadState {
    //PoolStruct pool;
    BackoffStruct backoff;
    bucketList *blist;
    retiredList *rlist;  //thread's list of retired nodes
    int rcount;
    long id; 	//thread's id
} LFStackThreadState;

void LFStackInit(LFStack *l,int thread_n);
void LFStackThreadStateInit(LFStackThreadState *th_state,int min_back, int max_back,int id);
void LFStackPush(LFStack *l, LFStackThreadState *th_state, ArgVal arg);
RetVal LFStackPop(LFStack *l, LFStackThreadState *th_state,int thread_n);

#endif