#ifndef _HP_MSQUEUE_H_
#define _HP_MSQUEUE_H_

#include <config.h>
#include <primitives.h>
#include <backoff.h>
#include <fastrand.h>
//#include <pool.h>
#include <threadtools.h>
#include <queue-stack.h>
#include <hazard.h>
#include <structure_allignment.h>
#include <bucket.h>



typedef struct MSQueue {
    volatile Node *head CACHE_ALIGN;
    volatile Node *tail CACHE_ALIGN;
    int instance  __attribute__ ((aligned (CACHE_LINE_SIZE)));
    int hp_offset;
} MSQueue;

typedef struct MSQueueThreadState {
   // PoolStruct pool;
	bucketList *blist;
    BackoffStruct backoff;
    retiredList *rlist;
    int rcount;
    long id;
} MSQueueThreadState;

void MSQueueInit(MSQueue *l,int thread_n);
void MSQueueThreadStateInit(MSQueueThreadState *th_state, int min_back,	int max_back,int id);
void MSQueueEnqueue(MSQueue *l, MSQueueThreadState *th_state, ArgVal arg);
RetVal MSQueueDequeue(MSQueue *l, MSQueueThreadState *th_state,int thread_num);

#endif
