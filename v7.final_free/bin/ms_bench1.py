import sys
import subprocess
from statistics import mean
import numpy as np
import matplotlib.pyplot as plt

hazard=list(range(0,5));
no_hazard=list(range(0,5));
#	calculating with hazard pointers
i=1;
print("\t -----Testing queue----- '\t");
while(i<6):
	print("Calculating hazard pointers case using ",2**i," threads ...");
	results=list(range(0,10));
	for x in range(10):			
		proc=subprocess.Popen(['./test1_hazard_bench.run',str(i*2)],stdout=subprocess.PIPE,stdin=subprocess.PIPE);		
		results[x]=int(proc.stdout.read());		
	results.remove(max(results));
	results.remove(min(results));
	hazard[i-1]=mean(results);
	i=i+1;
print("Result:\n"+str(hazard));
