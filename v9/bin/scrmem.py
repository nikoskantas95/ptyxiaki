#!/usr/bin/python3
import csv, time
from os import system
from sys import argv

if len(argv) < 2:
	print('Usage: ' + argv[0] + ' exp_name')
	exit()

EXP_NAME = str(argv[1])

with open('memrec.csv') as f, open('memrec_temp.csv','w+') as f2:
	r = csv.reader(f, delimiter='\t')
	w = csv.writer(f2)
	lines = list(r)

	# Throw away empty values
	for line in lines:
		timeCol = line[0]
		memCol = line[1]
		if timeCol != '' and memCol != '':
			w.writerow(line)

with open('memrec_temp.csv') as f, open('memrec_fin.csv','w') as f2:
	r = csv.reader(f)
	w = csv.writer(f2)
	lines = list(r)

	# Compute millions
	lines[0][0] = '#TIME'
	lines[0][1] = 'GB'
	for line in lines[1:]:
		line[1] = round(int(line[1])/1000000,2)

	# Write to file
	w.writerows(lines)

# Cleanup
system('rm memrec_temp.csv')

# Archive
DATETIME = time.strftime("%Y-%m-%d-%H:%M")
system('mv memrec.csv experiments/raw/expmem' + '_' + DATETIME + '_' + EXP_NAME + '_raw.csv')
system('mv memrec_fin.csv experiments/expmem' + '_' + DATETIME + '_' + EXP_NAME + '.csv')
