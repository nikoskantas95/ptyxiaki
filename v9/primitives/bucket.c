#include <bucket.h>
#include <math.h>

const int MEM_SIZE = 8192;



void init_bucket(bucketList **blist,int obj_size){
	static int b_id=0;
//	remove from comment for memory values
	//FAA32(&total_buckets,1);
	bucketStruct *new_bucket;
	int allignment,bid;
	if(*blist== NULL){
		allignment=getAllignment(sizeof(bucketStruct));
		bid=0;
	}
	else{
		allignment=(*blist)->bucket->allignment;
		bid=(*blist)->bucket->id+1;
	}
	new_bucket=getAlignedMemory(allignment,sizeof(bucketStruct));
	//*blist)->bucket_size=5;
	//new_bucket=malloc(sizeof(struct bucketStruct));
	new_bucket->available_zones=MEM_SIZE;
	new_bucket->obj_size=obj_size;
	new_bucket->index=0;
	new_bucket->id=bid;
	new_bucket->allignment=allignment;

	//printf("Id is %d\n",new_bucket->id);
///	printf("Bucket size is %d\n",sizeof(bucketStruct));
	//void *ptr=(void*)new_bucket+8192*obj_size;
	//printf("Size is %d\n",obj_size);
	//printf("Bucket at %lu with id %d\n",new_bucket,new_bucket->id);
	//printf("ID %d Counter is at %lu\n",new_bucket->id,(void*)&new_bucket->available_zones);
	insertBucket(blist,new_bucket);
}

/*returns the next bucket in the list that has avaiable space */
bucketList *getAvailableBucket(bucketList **blist){

	bucketList *curr=*blist;
//	printf("%d",(curr)->bucket->id);
	//	exit(0);
	while(curr!=NULL){
	//	printf("Searching...\n");
	//	printf("=%d=\n",(*blist)->bucket);
		//printf("Counter %d\n",curr->bucket->available_zones);
		if(curr->bucket->available_zones!=0){
		//	printf("Found %d\n",curr->bucket->id);
			return curr;
		}
		curr=curr->next;
	}
//printf("Not found!\n");
	return NULL;
}

int getIndex(int bitmap[]){
	int i;
	for(i=0;i<MEM_SIZE;i++){
	//	printf("[%d]...[%d]\n",bitmap[i]),bitmap[i+1];
		if(bitmap[i]==0){
				//bitmap[i]=1;
			//printf("offset: %d\n",i);
			return i;
		}
	}
//	printf("?????");
	return -1;
}

void *realloc_object(bucketList **blist){
	//printf("Realloc...\n");f()
	int offset;
	bucketList *reuse=getAvailableBucket(blist);
	//if(reuse!=NULL)
		//printf("Got the bucket: %d\n",(reuse->bucket->id));

	if(reuse!=NULL){ 	//a bucket with available space exists
	//	printf("searching index\n");
	//printf("[%lu]\n",reuse->bucket);
		//	cross(*blist);
			offset=getIndex(reuse->bucket->memory_bitmap);
			reuse->bucket->memory_bitmap[offset]=1;
		//	printf("Decrement available zones at%lu\n",&(reuse->bucket->available_zones));

			reuse->bucket->available_zones--;
			//printf("----%d----\n",reuse->bucket->available_zones);
			if(offset==-1){
				printf("This shouldnt happen");
				exit(0);
			}
	}
	else{
		//cross(*blist);
	//	printf("New_bucket\n");
		int size=(*blist)->bucket->obj_size;
		init_bucket(blist,size);

	//	exit(0);
	 	reuse=*blist;
		offset=getIndex(reuse->bucket->memory_bitmap);
		reuse->bucket->memory_bitmap[offset]=1;
		reuse->bucket->available_zones--;


	}
//	printf("Bucket id:%d with offset : %d\n",(reuse->bucket->id),offset);
	long int val= (void *) &(reuse)->bucket->memory_pool[offset * (reuse)->bucket->obj_size];
	//printf("New node at: %lu\n",val);
	//printf("Bit:%lu\n",&reuse->bucket->memory_bitmap[offset]);
	return (void *) &(reuse)->bucket->memory_pool[offset * (reuse)->bucket->obj_size];
}

void *alloc_object(bucketList **blist){

	int offset;
	if((*blist)->bucket->index==MEM_SIZE){
		//printf("New\n");

		int size=(*blist)->bucket->obj_size;
		init_bucket(blist,size);
	}
	offset=(*blist)->bucket->index;
	//printf("Index %d\n",(*blist)->bucket->index);
	(*blist)->bucket->index += 1;


	//printf("Done");
	//printf("offset:%d\n",offset);
	//printf("Index at array:%lu\n",&blist->bucket->memory_pool[offset * blist->bucket->obj_size]);

	long int val= (void *) &(*blist)->bucket->memory_pool[offset * (*blist)->bucket->obj_size];
	//printf("Val is %lu -->%lu\n",val,val/pow(2,18));
	return (void *) &(*blist)->bucket->memory_pool[offset * (*blist)->bucket->obj_size];
}






void insertBucket(bucketList **blist,bucketStruct *new_bucket){
	//printf("New bucket counter at %lu\n",(new_bucket+128*16));
	bucketList *temp=NULL,*prev=NULL;
	temp=getAlignedMemory(CACHE_LINE_SIZE,sizeof(bucketList));
	temp->bucket=new_bucket;
	temp->next=NULL;
	temp->prev=NULL;
	if((*blist)!= NULL){
		//printf("head\n");
		(*blist)->prev=temp;
		temp->next=(*blist);
	}	//temp->next=*blist;
	*blist=temp;
		//printf("New blist\n");
		//printf("New blist at %lu\n",(*blist)->bucket);

}

void setBitmapAddress(void *address,int allignment,int offset){
	long int start=((long int)address);
	void *val;
	long int aux,bitmap,index;
	start=start/allignment;
	start=start*allignment;
	aux=((long int)address);
	index=(aux-start)/offset;
	bucketStruct *buck= (bucketStruct *) start;
	bitmap=start+MEM_SIZE*offset+index+sizeof(int);
	//buck->memory_bitmap[index]
	//printf("%lu==%lu\n",&(buck->memory_bitmap[index]),bitmap);

	/*if(!CAS32(&(buck->memory_bitmap[index]),1,0)){
		printf("CAS failed\n");
		exit(0);
	}*/
	//printf("%MEMORY should be 1==%d\n",buck->memory_bitmap[index]);
	buck->memory_bitmap[index]=0;
	//printf("%MEMORY should be 0==%d\n",buck->memory_bitmap[index]);
	reuseZone(start+MEM_SIZE*offset);

//	printf("Bucket starts at %ld\n",start);
//	end=(aux-start);
	//end=start+MEM_SIZE*offset;
  //printf("[%lu]\n",aux);

	//val=(void *) (aux + offset*MEM_SIZE);
	//printf("Struct starts at %lu\n",val);
	//printf("Counter at %lu\n\n",val);
	//deleteZone(val);
	//return  val;
}

void *getAddress(void *address,int allignment,int offset){
	long int aux=((long int)address);
	void *val;
	//printf("Size is %d \n",obj_size);
	//printf("Begin %lu\n",address);
	//printf("Cast %lu  / %d\n",aux,obj_size);
	aux=aux/allignment;
	//printf("Inter %lu\n",aux);
	aux=aux*allignment;
	val=(void *) (aux + offset*MEM_SIZE);
//	printf("Struct starts at %lu\n",aux);
	//printf("Counter at %lu\n\n",val);
	//deleteZone(val);
	return  val;

}

void reuseZone(void *available_zones_cnt){
	//printf("%lu ++++\n",available_zones_cnt);
	FAA32(available_zones_cnt,1);
}

void deleteZone(void *counter){
	//printf("Trying to delete from memory:%lu\n\n",counter);
	FAA32(counter,-1);
	//printf("hhh");
	//printf("%d\n",*counter);
}

void error_checking(bucketList *blist){
	int i;
	int cnt;
	while(blist!=NULL){
		cnt=0;
		for(i=0;i<MEM_SIZE;i++){
			cnt=cnt+(blist)->bucket->memory_bitmap[i];
		}
		if(cnt!=MEM_SIZE-blist->bucket->available_zones){
		printf("Error!	~~~~~>%d vs. %d<~~~~~\n",cnt,MEM_SIZE-blist->bucket->available_zones);
		}
		blist=blist->next;
	}
}
void cross(bucketList *blist){
	int i;
	while(blist!=NULL){
		printf("Bucket id:%d\n",(blist)->bucket->id);
		printf("Available zones:%d\n",blist->bucket->available_zones);
		for(i=0;i<MEM_SIZE;i++){
			printf("[%d]->%d\n",i,blist->bucket->memory_bitmap[i]);
		}


		blist=blist->next;
	}
}

/*void bucket_freemem(void *node,int bucket_size,int node_size){
	int node_allignment,node_offset;
	void *available_zones_cnt,*bucket;
	node_allignment=getAllignment(bucket_size);
	node_offset=node_size;
	available_zones_cnt=getAddress(node,node_allignment,node_offset);
	deleteZone(available_zones_cnt);
	bucket=available_zones_cnt- node_offset*8192;
	if(bucket->available_zones == 0){
		free(bucket);
	}

}*/

void deleteBucket(bucketList **blist){
	bucketList *curr=*blist,*prev,*to_free;
	static int cnt=0;
	//printf("??");
	while(curr!=NULL){
			printf("Bucket id:%d	Zones:%d\n",(curr)->bucket->id,(curr)->bucket->available_zones);
				printf("%d\n",(*blist)->bucket->available_zones);
		//printf("%d\n",cnt);
		//cnt++;
		if(curr->bucket->available_zones == 0){
			to_free=curr;
			if(curr == *blist){
				printf("Deleting head\n");
				(*blist)=(*blist)->next;
				(*blist)->prev=NULL;
				//FAA32(&buckets_freed,1);
				free(to_free);
			}
			else{
				curr->prev->next=curr->next;
				if(curr->next != NULL)
					curr->next->prev=curr->prev;
					//printf("Freeing memory\n");
					//FAA32(&buckets_freed,1);
					free(to_free);
			}
		}

		curr=curr->next;
	}




}

int getAllignment(int allignment){
	static int cnt;
	//cnt++;
	float result;
	result=log2(allignment);
	//printf("Res:%d\n",(int)ceil(result));
	//printf("getAllignment: %d\n",cnt);
	return pow(2,(int) ceil(result));



}
