#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <stdint.h>

#include <config.h>
#include <primitives.h>
#include <backoff.h>
#include <fastrand.h>
#include <threadtools.h>
#include <hp_lfstack.h>
#include <barrier.h>

#define ACTION 1000000

LFStack stack1 CACHE_ALIGN;
int64_t d1 CACHE_ALIGN, d2;
int MIN_BAK=1, MAX_BAK=2;
Barrier bar;
int thread_num;


inline static void *execute(void* Arg) {
	LFStackThreadState *th_state;
    long i;
    long id = (long) Arg;
    long rnum,value,consume=0,pop1,pop2=0;
    volatile long j;
    th_state = getAlignedMemory(CACHE_LINE_SIZE, sizeof(LFStackThreadState));
    LFStackThreadStateInit(th_state, MIN_BAK, MAX_BAK,id);
  	 BarrierWait(&bar);
    if (id == 0)
        d1 = getTimeMillis();
  	for (i = 0; i < ACTION/thread_num; i++) {  
  		//printf("%d\n",i);      
    	value=fastRandomRange(1,10);
       	LFStackPush(&stack1, th_state,value);
       	//alue=fastRandomRange(1,10);
       //	LFStackPush(&stack2, th_state,value);           

           
    	//value=fastRandomRange(1,10);
       	pop1=LFStackPop(&stack1, th_state,thread_num);
       //	pop2=LFStackPop(&stack2, th_state,thread_num);
        consume=consume+pop1;        

    }
 	 //printf("Thread %d consumed:%ld\n",id,consume);
    return NULL;

}

int main(int argc,char *argv[]){	
	char *ptr;
	//int thread_num;
	int *thread_aux;
	pthread_t *threads;		
	thread_num=atoi(argv[1]);
	int i=0;	
	threads=malloc(thread_num*sizeof(pthread_t));	
	thread_aux=malloc(thread_num*sizeof(int));	
	initHPRec(thread_num,1);
	LFStackInit(&stack1,thread_num);	
  BarrierInit(&bar, thread_num);   
/*	for(i=0;i<thread_num;i++){
		thread_aux[i]=i;		
	}
	for(i=0;i<thread_num;i++){
		pthread_create(&threads[i],NULL,execute,&thread_aux[i]);		
	}

	for(i=0;i<thread_num;i++){
		pthread_join(threads[i],NULL);
	}*/
  StartThreadsN(thread_num, execute, _DONT_USE_UTHREADS_);
  JoinThreadsN(thread_num);
	d2 = getTimeMillis();
	//printf("time: %d (ms)\tthroughput: %.2f (millions ops/sec)\t\n\n", (int) (d2 - d1), 2*RUNS*thread_num/(1000.0*(d2 - d1)));
	printf("%d\n",(int)(d2-d1));


	return 0;
}