#!/bin/bash
POWER=1;
for ((i=2;i<64;i=2**POWER)) ;do
	echo "Calculating hazard pointers case using $i threads ..."; 
	for((j=1;j<=10;j++)) ;do
		echo -n "#";
		../bin/test1_hazard_bench.run $i >> ms_results.txt;
		
	done;
	((POWER++));
	echo -e "\n";
done;

POWER=1;
for ((i=2;i<64;i=2**POWER)) ;do
	echo "Calculating without using hazard pointers case using $i threads ..."; 
	for((j=1;j<=10;j++)) ;do
		echo -n "#";
		../bin/test1_nohazard_bench.run $i >> ms_results.txt;
		
	done;
	((POWER++));
	echo -e "\n";
done;
