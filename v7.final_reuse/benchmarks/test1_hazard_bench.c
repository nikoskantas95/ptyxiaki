#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <stdint.h>
#include <bucket.h>
#include <config.h>
#include <primitives.h>
#include <backoff.h>
#include <fastrand.h>
#include <threadtools.h>
#include <hp_msqueue.h>
#include <barrier.h>

#define ACTION 10000

MSQueue q1 CACHE_ALIGN,q2 CACHE_ALIGN;
int64_t d1 CACHE_ALIGN, d2;
int MIN_BAK=1, MAX_BAK=2;
Barrier bar;
int thread_num;

int remaining(){
	int rem=0,out=0;
	MSQueueThreadState *th_state;
	th_state = getAlignedMemory(CACHE_LINE_SIZE, sizeof(MSQueueThreadState));
	MSQueueThreadStateInit(th_state, MIN_BAK, MAX_BAK,0);
	while(1){

		out=MSQueueDequeue(&q1, th_state,1);
		rem++;
		if(out == -1)
			return rem;
	}
}

inline static void *execute(void* Arg) {
	MSQueueThreadState *th_state;
    long i;
   long id = (long) Arg;
    int aux;
    long rnum,value,consume=0,deq1=0,deq2=0,enq=0,rem=0;
    volatile long j;
    //exit(0);
   // printf("%d\n",id);
  // exit(0);
    th_state = getAlignedMemory(CACHE_LINE_SIZE, sizeof(MSQueueThreadState));
    MSQueueThreadStateInit(th_state, MIN_BAK, MAX_BAK,id);
   	BarrierWait(&bar);
   	aux=id+1;
   	//printf("Id is %d\n",aux);
    if (id == 0){
    	//printf("timing %d\n",id);
        d1 = getTimeMillis();
    }
  	for (i = 0; i < ACTION/thread_num; i++) {
  		//printf("%d\n",i);
    	//value=fastRandomRange(1,10);
    	value=1;
    	enq=enq+value;
    	MSQueueEnqueue(&q1, th_state,value);

      }


    	//value=fastRandomRange(1,10);
      for(i=0;i< ACTION/thread_num;i++){
       	deq1=MSQueueDequeue(&q1, th_state,thread_num);
       //	printf("Deq:%d\n",deq1);
      	//deq2=MSQueueDequeue(&q2, th_state,thread_num);
      	consume=consume+deq1;
			}
				printf("Thread %d consumed %d\n",id,consume);

	return NULL;
}

int main(int argc,char *argv[]){
	char *ptr;
	//int thread_num;
	int *thread_aux;
	pthread_t *threads;
	thread_num=atoi(argv[1]);
	int i=0;
	threads=malloc(thread_num*sizeof(pthread_t));
	thread_aux=malloc(thread_num*sizeof(int));
	initHPRec(thread_num,1);
	//printf("Start\n");
	MSQueueInit(&q1,thread_num);
	//printf("Done\n");
	//MSQueueInit(&q2);

    BarrierInit(&bar, thread_num);
	/*for(i=0;i<thread_num;i++){
		thread_aux[i]=i;
	}
	for(i=0;i<thread_num;i++){
		pthread_create(&threads[i],NULL,execute,&thread_aux[i]);
	}

	for(i=0;i<thread_num;i++){
		pthread_join(threads[i],NULL);
	}

	//printQ(q1);*/
	StartThreadsN(thread_num, execute, _DONT_USE_UTHREADS_);
  JoinThreadsN(thread_num);
	d2 = getTimeMillis();

	//printf("time: %d (ms)\tthroughput: %.2f (millions ops/sec)\t\n\n", (int) (d2 - d1), 2*RUNS*thread_num/(1000.0*(d2 - d1)));
	///printf("Total buckets:%d \nTotal memory dealocated:%d\n",total_buckets,buckets_freed);
	//int rem=remaining();
	//remaining();
	//printf("Total buckets created:%d\n",total_buckets);
	//printf("Buckets freed:%d\n",buckets_freed);

	printf("%d\n",(int)(d2-d1));


	return 0;
}
