#ifndef _BUCKET_H_
#define _BUCKET_H_

#include <primitives.h>
#include <queue-stack.h>
#include <primitives.h>

volatile int buckets_freed;
volatile int total_buckets;
volatile int total_nodes_reused;


typedef struct bucketStruct{
	char memory_pool[8192*sizeof(Node)];
	volatile int available_zones;
	int memory_bitmap[8192];
	int index;
	int obj_size; //????
	int id;
	int allignment;
}bucketStruct;

typedef struct bucketList{

  bucketStruct *bucket;
	struct bucketList *next;
	struct bucketList *prev;

}bucketList;



void init_bucket(bucketList **blist,int obj_size);
void *alloc_object(bucketList **blist);
void insertBucket(bucketList **blist,bucketStruct *new_bucket);
void *getAddress(void *address,int allignment,int offset);
void deleteZone(void *counter);
void deleteBucket(bucketList **blist);
int getAllignment(int allignment);
void cross(bucketList *blist);
void *realloc_object(bucketList **blist);
int getIndex(int bitmap[]);
bucketList *getAvailableBucket(bucketList **blist);
void setBitmapAddress(void *address,int allignment,int offset);
void *getAddress(void *address,int allignment,int offset);
void error_checking(bucketList *blist);



#endif
