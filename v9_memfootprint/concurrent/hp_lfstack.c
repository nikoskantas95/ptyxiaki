#include <hp_lfstack.h>

inline void LFStackInit(LFStack *l,int thread_n) {
    l->top = null;
    total_instances++;
    l->instance=total_instances;    
    l->offset=(l->instance-1)*2*thread_n;
    //l->offset=HPStruct[l->instance*thread_n*2]; if offset is pointer
    FullFence();
}

inline void LFStackThreadStateInit(LFStackThreadState *th_state,int min_back, int max_back,int id) {
    init_backoff(&th_state->backoff, min_back, max_back, 1);
    //init_pool(&th_state->pool, sizeof(Node));
    init_bucket(&th_state->blist,sizeof(Node));
    th_state->id=id;
}

inline void LFStackPush(LFStack *l, LFStackThreadState *th_state, ArgVal arg) {
    Node *n;

   // n = alloc_obj(&th_state->pool);
    reset_backoff(&th_state->backoff);
    //n=getAlignedMemory(64,sizeof(Node));
    n=alloc_object(&th_state->blist);  
    n->val=arg;
    n->val = arg;
    do {
        Node *old_top = (Node *) l->top;   // top is volatile
        n->next = old_top;
        if (CASPTR(&l->top, old_top, n) == true)
            break;
        else backoff_delay(&th_state->backoff);
    } while(true);
}

inline RetVal LFStackPop(LFStack *l, LFStackThreadState *th_state,int thread_n) {
    int hp_offset=(l->offset + th_state->id*2);
    reset_backoff(&th_state->backoff);
    do {
        Node *old_top = (Node *) l->top;
        if (old_top == null)
            return (RetVal)INT_MIN;
     //  HPRecords[th_state->id][2*l->instance]=old_top;
         HPStruct.HPRecords[hp_offset]=old_top;
        if(l->top != old_top){
            backoff_delay(&th_state->backoff);
            continue;
        }
        if(CASPTR(&l->top, old_top, old_top->next)){
           RetireNode(old_top,&(th_state->rlist),&(th_state->rcount),thread_n,&(th_state->blist));
            return old_top->val;
        }
        else backoff_delay(&th_state->backoff);
    } while (true) ;
}
