#include <bucket.h>
#include <math.h>

const int MEM_SIZE = 8192;


void init_bucket(bucketList **blist,int obj_size){
	static int b_id=0;
	//remove from comment for memory values
	FAA32(&total_buckets,1);
	bucketStruct *new_bucket;
	int allignment,bid;
	if(*blist== NULL){
		allignment=getAllignment(sizeof(bucketStruct));
		bid=0;
	}
	else{
		allignment=(*blist)->bucket->allignment;
		bid=(*blist)->bucket->id+1;
	}
	new_bucket=getAlignedMemory(allignment,sizeof(bucketStruct));
	//*blist)->bucket_size=5;
	//new_bucket=malloc(sizeof(struct bucketStruct));
	new_bucket->available_zones=MEM_SIZE;
	new_bucket->obj_size=obj_size;
	new_bucket->index=0;
	new_bucket->id=bid;
	new_bucket->allignment=allignment;

	//printf("Id is %d\n",new_bucket->id);
///	printf("Bucket size is %d\n",sizeof(bucketStruct));
	//void *ptr=(void*)new_bucket+8192*obj_size;
	//printf("Size is %d\n",obj_size);
	//printf("Bucket at %lu with id %d\n",new_bucket,new_bucket->id);
	//printf("ID %d Counter is at %lu\n",new_bucket->id,(void*)&new_bucket->available_zones);
	insertBucket(blist,new_bucket);
}

void *alloc_object(bucketList **blist){
	//printf("Available :%d\n",(*blist)->bucket->available_zones);
	//printf("ffgf");
	//printf("Counter location %lu\n",&(*blist)->bucket->available_zones);
	//bucketList *aux=blist;
	//printf("Available:%d\n",(*blist)->bucket->available_zones);
	int offset;
	if((*blist)->bucket->index==MEM_SIZE){
		//printf("New\n");
		int size=(*blist)->bucket->obj_size;
		init_bucket(blist,size);
	}
	offset=(*blist)->bucket->index;
	//printf("Index %d\n",(*blist)->bucket->index);
	(*blist)->bucket->index += 1;


	//printf("Done");
	//printf("offset:%d\n",offset);
	//printf("Index at array:%lu\n",&blist->bucket->memory_pool[offset * blist->bucket->obj_size]);

	long int val= (void *) &(*blist)->bucket->memory_pool[offset * (*blist)->bucket->obj_size];
	//printf("Val is %lu -->%lu\n",val,val/pow(2,18));
	return (void *) &(*blist)->bucket->memory_pool[offset * (*blist)->bucket->obj_size];
}






void insertBucket(bucketList **blist,bucketStruct *new_bucket){
	//printf("New bucket counter at %lu\n",(new_bucket+128*16));
	bucketList *temp=NULL,*prev=NULL;
	temp=getAlignedMemory(CACHE_LINE_SIZE,sizeof(bucketList));
	temp->bucket=new_bucket;
	temp->next=NULL;
	temp->prev=NULL;
	if((*blist)!= NULL){
		//printf("head\n");
		(*blist)->prev=temp;
		temp->next=(*blist);
	}	//temp->next=*blist;
	*blist=temp;
		//printf("New blist\n");
		//printf("New blist at %lu\n",(*blist)->bucket);

}

void *getAddress(void *address,int allignment,int offset){
	long int aux=((long int)address);
	void *val;
	//printf("Size is %d \n",obj_size);
	//printf("Begin %lu\n",address);
	//printf("Cast %lu  / %d\n",aux,obj_size);
	aux=aux/allignment;
	//printf("Inter %lu\n",aux);
	aux=aux*allignment;
	val=(void *) (aux + offset*MEM_SIZE);
	//printf("Struct starts at %lu\n",val);
	//printf("Counter at %lu\n\n",val);
	//deleteZone(val);
	return  val;

}

void deleteZone(void *counter){
	//printf("Trying to delete from memory:%lu\n\n",counter);
	FAA32(counter,-1);
	//printf("hhh");
	//printf("%d\n",*counter);
}

void cross(bucketList *blist){
	while(blist!=NULL){
		printf("Bucket id:%d\n",(blist)->bucket->id);
		blist=blist->next;
	}
}

/*void bucket_freemem(void *node,int bucket_size,int node_size){
	int node_allignment,node_offset;
	void *available_zones_cnt,*bucket;
	node_allignment=getAllignment(bucket_size);
	node_offset=node_size;
	available_zones_cnt=getAddress(node,node_allignment,node_offset);
	deleteZone(available_zones_cnt);
	bucket=available_zones_cnt- node_offset*8192;
	if(bucket->available_zones == 0){
		free(bucket);
	}

}*/

void deleteBucket(bucketList **blist){
	bucketList *curr=*blist,*prev,*to_free;
	static int cnt=0;
	//printf("??");
	while(curr!=NULL){
			//printf("Bucket id:%d\n",(curr)->bucket->id);
		//printf("%d\n",cnt);
		//cnt++;
		if(curr->bucket->available_zones == 0){
			to_free=curr;
			if(curr == *blist){
				printf("Deleting head\n");
				(*blist)=(*blist)->next;
				(*blist)->prev=NULL;
				FAA32(&buckets_freed,1);
				free(to_free);
			}
			else{
				curr->prev->next=curr->next;
				if(curr->next != NULL)
					curr->next->prev=curr->prev;
					//printf("Freeing memory\n");
					FAA32(&buckets_freed,1);
					free(to_free);
			}
		}

		curr=curr->next;
	}




}

int getAllignment(int allignment){
	static int cnt;
	//cnt++;
	float result;
	result=log2(allignment);
	//printf("Res:%d\n",(int)ceil(result));
	//printf("getAllignment: %d\n",cnt);
	return pow(2,(int) ceil(result));



}
