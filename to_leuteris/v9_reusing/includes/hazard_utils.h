#ifndef HAZARD_UTILS_H
#define _HAZARD_UTILS_H_
#include <stdio.h>
#include <stdlib.h>
#include <system.h>

void merge(void *array[],int low,int mid,int high);
void mergeShort(void *array[],int low,int high);
void swap(void **xp, void **yp);
void bubbleSort(void *array[],int size);
int comperator (const void * a, const void * b);
void quickSort(void *array[],int first,int last);
int partition (void *arr[], int low, int high);




#endif