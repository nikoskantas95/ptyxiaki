#include <bucket.h>
#include <math.h>

const int MEM_SIZE = 8192;



void create_bucket(bucketList **blist,int obj_size){
	static int b_id=0;
//	remove from comment for memory values
	//FAA32(&total_buckets,1);
	bucketStruct *new_bucket;
	int allignment,bid;
	if(*blist== NULL){
		allignment=get_bucketAllignment(sizeof(bucketStruct));
		bid=0;
	}
	else{
		allignment=(*blist)->bucket->allignment;
		bid=(*blist)->bucket->id+1;
	}
	new_bucket=getAlignedMemory(allignment,sizeof(bucketStruct));
	new_bucket->memory_counter=MEM_SIZE;
	new_bucket->obj_size=obj_size;
	new_bucket->index=0;
	new_bucket->id=bid;
	new_bucket->allignment=allignment;
	insertBucket(blist,new_bucket);
}

/*returns the next bucket in the list that has avaiable space */
bucketList *getAvailableBucket(bucketList **blist){
	bucketList *curr=*blist;
	while(curr!=NULL){
		if(curr->bucket->memory_counter!=0){
			return curr;
		}
		curr=curr->next;
	}
	return NULL;

}

/*Searches the memory bitmap and returns the first index
	where an available memory for use resides*/
int get_memoryBitmap_index(int bitmap[]){
	int i;
	for(i=0;i<MEM_SIZE;i++){
		if(bitmap[i]==0){
			return i;
		}
	}
	return -1;
}

void *alloc_object(bucketList **blist){
	int offset;
	bucketList *reuse=getAvailableBucket(blist);
	if(reuse!=NULL){ 	//a bucket with available space exists
			offset=get_memoryBitmap_index(reuse->bucket->memory_bitmap);
			reuse->bucket->memory_bitmap[offset]=1;
			reuse->bucket->memory_counter--;
			if(offset==-1){
				printf("This shouldnt happen");
				exit(0);
			}
	}
	else{
		int size=(*blist)->bucket->obj_size;
		create_bucket(blist,size);
	 	reuse=*blist;
		offset=get_memoryBitmap_index(reuse->bucket->memory_bitmap);
		reuse->bucket->memory_bitmap[offset]=1;
		reuse->bucket->memory_counter--;


	}
	long int val= (void *) &(reuse)->bucket->memory_pool[offset * (reuse)->bucket->obj_size];

	return (void *) &(reuse)->bucket->memory_pool[offset * (reuse)->bucket->obj_size];
}
/* -----------legacy------

void *alloc_object(bucketList **blist){

	int offset;
	if((*blist)->bucket->index==MEM_SIZE){


		int size=(*blist)->bucket->obj_size;
		create_bucket(blist,size);
	}
	offset=(*blist)->bucket->index;
	(*blist)->bucket->index += 1;

	long int val= (void *) &(*blist)->bucket->memory_pool[offset * (*blist)->bucket->obj_size];
	return (void *) &(*blist)->bucket->memory_pool[offset * (*blist)->bucket->obj_size];
}*/


void insertBucket(bucketList **blist,bucketStruct *new_bucket){
	//printf("New bucket counter at %lu\n",(new_bucket+128*16));
	bucketList *temp=NULL,*prev=NULL;
	temp=getAlignedMemory(CACHE_LINE_SIZE,sizeof(bucketList));
	temp->bucket=new_bucket;
	temp->next=NULL;
	temp->prev=NULL;
	if((*blist)!= NULL){

		(*blist)->prev=temp;
		temp->next=(*blist);
	}
	*blist=temp;

}

void bucket_reuse_memory(void *address,int allignment,int offset){
	long int start=((long int)address);
	void *val;
	long int aux,bitmap,index;
	start=start/allignment;
	start=start*allignment;
	aux=((long int)address);
	index=(aux-start)/offset;
	bucketStruct *buck= (bucketStruct *) start;
	//bitmap=start+MEM_SIZE*offset+index+sizeof(int);
	buck->memory_bitmap[index]=0;//change bitmap
	increment_memory_counter(start+MEM_SIZE*offset);//memory can be reused
}
/* ----legacy--------
void *getAddress(void *address,int allignment,int offset){
	long int aux=((long int)address);
	void *val;
	aux=aux/allignment;
	aux=aux*allignment;
	val=(void *) (aux + offset*MEM_SIZE);
	return  val;

}*/

void increment_memory_counter(void *memory_counter_cnt){
	FAA32(memory_counter_cnt,1); //FAA32 is a fatch and add implementation not binded by different compiler versions.
}
/* -----------------legacy --------------
void deleteZone(void *counter){
	//printf("Trying to delete from memory:%lu\n\n",counter);
	FAA32(counter,-1);
	//printf("hhh");
	//printf("%d\n",*counter);
}
*/

/*Debugging purposes*/
void error_checking(bucketList *blist){
	int i;
	int cnt;
	while(blist!=NULL){
		cnt=0;
		for(i=0;i<MEM_SIZE;i++){
			cnt=cnt+(blist)->bucket->memory_bitmap[i];
		}
		if(cnt!=MEM_SIZE-blist->bucket->memory_counter){
		printf("Error!	~~~~~>%d vs. %d<~~~~~\n",cnt,MEM_SIZE-blist->bucket->memory_counter);
		}
		blist=blist->next;
	}
}
/*Debugging purposes*/
void cross(bucketList *blist){
	int i;
	while(blist!=NULL){
		printf("Bucket id:%d\n",(blist)->bucket->id);
		printf("Available zones:%d\n",blist->bucket->memory_counter);
		for(i=0;i<MEM_SIZE;i++){
			printf("[%d]->%d\n",i,blist->bucket->memory_bitmap[i]);
		}


		blist=blist->next;
	}
}

/*void bucket_freemem(void *node,int bucket_size,int node_size){
	int node_allignment,node_offset;
	void *memory_counter_cnt,*bucket;
	node_allignment=get_bucketAllignment(bucket_size);
	node_offset=node_size;
	memory_counter_cnt=getAddress(node,node_allignment,node_offset);
	deleteZone(memory_counter_cnt);
	bucket=memory_counter_cnt- node_offset*8192;
	if(bucket->memory_counter == 0){
		free(bucket);
	}

}*/

void deleteBucket(bucketList **blist){
	bucketList *curr=*blist,*prev,*to_free;
	static int cnt=0;
	//printf("??");
	while(curr!=NULL){
			printf("Bucket id:%d	Zones:%d\n",(curr)->bucket->id,(curr)->bucket->memory_counter);
				printf("%d\n",(*blist)->bucket->memory_counter);
		//printf("%d\n",cnt);
		//cnt++;
		if(curr->bucket->memory_counter == 0){
			to_free=curr;
			if(curr == *blist){
				printf("Deleting head\n");
				(*blist)=(*blist)->next;
				(*blist)->prev=NULL;
				//FAA32(&buckets_freed,1);
				free(to_free);
			}
			else{
				curr->prev->next=curr->next;
				if(curr->next != NULL)
					curr->next->prev=curr->prev;
					//printf("Freeing memory\n");
					//FAA32(&buckets_freed,1);
					free(to_free);
			}
		}

		curr=curr->next;
	}




}

int get_bucketAllignment(int allignment){
	static int cnt;
	//cnt++;
	float result;
	result=log2(allignment);
	//printf("Res:%d\n",(int)ceil(result));
	//printf("get_bucketAllignment: %d\n",cnt);
	return pow(2,(int) ceil(result));



}
