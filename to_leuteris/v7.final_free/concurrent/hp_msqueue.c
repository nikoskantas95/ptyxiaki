#include <hp_msqueue.h>


void MSQueueInit(MSQueue *l,int thread_n) {
    Node *p = getMemory(sizeof(Node));
  //  MSQueue_allign=getAllignment(sizeof(bucketStruct));
    p->next = null;
    l->head = p;
    l->tail = p;
    l->instance=total_instances;
    total_instances++;
    l->hp_offset=2*(thread_n-1)*l->instance; //arithmetic +0/1
    FullFence();

}

void MSQueueThreadStateInit(MSQueueThreadState *th_state, int min_back, int max_back,int id) {
    init_backoff(&th_state->backoff, min_back, max_back, 1);
  //  init_pool(&th_state->pool, sizeof(Node));   //pool is useless here,allocation is different
    //th_state->blist=malloc(sizeof(bucketList));
    th_state->blist=NULL;
    th_state->rlist=NULL;
    create_bucket(&th_state->blist,sizeof(Node));
    th_state->rcount=0;
    th_state->id=id;

}

void MSQueueEnqueue(MSQueue *l, MSQueueThreadState *th_state, ArgVal arg) {
    Node *p;
    Node *next, *last;
    p=alloc_object(&th_state->blist);
    //printf("P %lu   node:%d",p,sizeof(Node));
    //p=malloc(sizeof(Node));
    p->val = arg;
    p->next = null;
   // printf("Size of node is %d\n",sizeof(Node));*/

    reset_backoff(&th_state->backoff);
    while (true) {
        last = (Node *)l->tail;
        //HPRecords[th_state->id][2*l->instance]=last;
        HPStruct.HPRecords[l->hp_offset]=last;
        if(l->tail != last){
          backoff_delay(&th_state->backoff);
          continue;
        }
        next = (Node *)last->next;
        if (last == l->tail) {
            if (next == null) {
               reset_backoff(&th_state->backoff);
                if (CASPTR(&last->next, next, p))
                    break;
            }
            else {
                CASPTR(&l->tail, last, next);
                backoff_delay(&th_state->backoff);
            }
        }
    }
    CASPTR(&l->tail, last, p);
}

RetVal MSQueueDequeue(MSQueue *l, MSQueueThreadState *th_state,int num_threads) {
    Node *first, *last, *next;
    Object value;
    //printf("Del\n");

    reset_backoff(&th_state->backoff);
    while (true) {
        first = (Node *)l->head;
        //printf("Head have %d\n",first->val);
       // HPRecords[th_state->id][2*l->instance]=first;
        HPStruct.HPRecords[l->hp_offset]=first;
        if(l->head != first){
          backoff_delay(&th_state->backoff);
          continue;
        }
        last = (Node *)l->tail;
        next = (Node *)first->next;
        // HPRecords[th_state->id][2*l->instance+1]=next;
        HPStruct.HPRecords[l->hp_offset+1]=next;
        if (first == l->head) {
             if (first == last) {
                 if (next == null){
                   HPStruct.HPRecords[l->hp_offset]=NULL;
                   HPStruct.HPRecords[l->hp_offset+1]=NULL;

                    return -1;
                }
                 CASPTR(&l->tail, last, next);
                 backoff_delay(&th_state->backoff);
             }
             else {
                  value = next->val;
                  if (CASPTR(&l->head, first, next))
                      break;
                  backoff_delay(&th_state->backoff);
             }
        }
     }
     //printf("Reting %p\n",next);
      //printf("Deqeue\n");
      RetireNode(next,&(th_state->rlist),&(th_state->rcount),num_threads,&(th_state->blist));
      //deleteBucket(&(th_state->blist));
      //printf("ID: %d Zones: %d\n",th_state->blist->bucket->id,th_state->blist->bucket->available_zones);
    // printf("Dequeue:%d\n",value);
     return value;
}
