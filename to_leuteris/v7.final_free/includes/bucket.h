#ifndef _BUCKET_H_
#define _BUCKET_H_

#include <primitives.h>
#include <queue-stack.h>
#include <primitives.h>

volatile int buckets_freed;
volatile int total_buckets;


typedef struct bucketStruct{
	char memory_pool[8192*sizeof(Node)];
	volatile int memory_counter; //counts total chuncks of memory being used
	int index; // current index at memory_pool.Must be different from memory_counter
	//for concurrency issues.Index shows how big the memory_pool currently is.
	//Memory counter shows how many indexes in the memory_pool can be used
	int obj_size; //used in alloc_object
	int id; //for debugging purposes
	int allignment;
}bucketStruct;

typedef struct bucketList{

  bucketStruct *bucket;
	struct bucketList *next;
	struct bucketList *prev;

}bucketList;



void create_bucket(bucketList **blist,int obj_size);
void *alloc_object(bucketList **blist);
void insertBucket(bucketList **blist,bucketStruct *new_bucket);
void *get_memory_counter_address(void *address,int allignment,int offset);
void decrement_memory_counter(void *counter);
void markFree_hazardous_memory(void *address,int allignment);
void clear_buckets(bucketList **blist);
int get_bucketAllignment(int allignment);
void cross(bucketList *blist);



#endif
